###  Sign in Process

Sign in with your BindTuning account. If you don't have any products, click on <a href="https://bindtuning.com/account/downloads">**Get Started Now**</a> and fill the form.

![loginbind.png](../image/loginbind.png)

___

After signing in with the BindTuning Account the next step is to login in the SharePoint environment. Sign in is available for **Office 365** and **SharePoint 2013/2016**. To switch between them you need to click on the corresponding icon for each SharePoint version.

There are two types of login available being **Custom Credentials** and **Credentials of the current user**.

**Credentials of the current user** requires the URL of the SharePoint Site Collection where you intend to install the products. After clicking in Sign In a new window will ask you to authenticate.

**Custom Credentials** requires additional information along with the SharePoint Site Collection URL in order to sign in, an email/username and its password. The major difference is the automation (no new windows asking to authenticate) with further logins necessary to perform certain actions in Provisioning.

<p class="alert alert-info">To use Provisioning with SharePoint On-Prem, the SharePoint Server Client Components SDK must be installed. More information on the requirements section.</p>

![login.png](../image/login.png)

___

After you Sign in on your SharePoint platform you'll see the home section of Provisioning there are some shortcuts that you can use to help you reach the desired tabs.

Currently you can access:

- [Install BindTuning Products](../features/web_parts.md) 
- [Install BindTuning Templates](../features/templates.md)
- [Automate 365](../features/automate365.md)

![shortcuts.png](../image/shortcuts.png)