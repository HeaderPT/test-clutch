On the top left corner, there is a **File** dropdown that is available with quick shortcuts to perform some actions.

The options available are: 

1. **Connect to another site**: It'll reload the application so you can connect to another site collection from your SharePoint. 
2. **Clear Session data**: It'll clear all the BindTuning session data stored on your Desktop.
3. **Exit**: It'll close the application on your Desktop.

![file.png](../image/file.png)