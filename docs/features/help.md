On the top left corner, there is a **Help** dropdown that is available with quick shortcuts to perform some actions.

The options available are: 

1. **Checking for updates**: From time to time Provisioning has new features or fixes available. Provisioning it will do the updates automatically for you but you can force it to check if there is an update.
2. **User Guide**: Open the BindTuning user guide for the Provisioning.
3. **Support**: Open the BindTuning Support website where you can find information related to the SharePoint and BindTuning products. You can also open a support ticket with your questions about the Provisioning or any BindTuning product.
4. **About**: Provides information about the current version of the Provisioning.

![clutchzip.png](../image/help_settings.png)