The Settings of the Provisioning allows you to add new API Keys to be used on Provisioning by adding a API Provisioning Key at the text area and then click on Add Key.
You can obtain a New API Key by clicking on Get New Key button. Each key added in the process is stored in the Provisioning software allowing for a later use.

There is a table providing you usage towards the keys that are stored in the the application. There is some information about the existing keys and allows each one of them to be activated or removed as the user wishes.
<p class="alert alert-info">One API Key must be active at all time.</p>
<p class="alert alert-info">Adding a new key or switching the active key will lead to the restart of the software.</p>
It'll also display information about the current key in use.

- Clearing login URLs autocomplete list - when you're at the login page on Office 365 you can clear the dropdown suggestions by clicking on clear.
- Activating Bing **Image of the Day**

![clutch_setup.png](../image/clutch_setup.png)