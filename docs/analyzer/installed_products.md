This section is available by clicking the Analyzer menu item followed by selecting the Installed Products option.

This section provides reports about the current products installed in the connected Site. The information is divided in three segments:

-	Tenant App Catalog
-	Site App Catalog
-	Solutions Gallery ( Exclusive to Classic )

<p class="alert alert-info">This feature is not available on the Admin Center Site.</p>

Products from Tenant and Site App Catalog can be uninstalled directly from Provisioning.
For the Solutions Gallery it is only possible to uninstall products that were installed or updated previously by Provisioning.
All the products related to the Solutions Gallery where it is not possible to uninstall, its uninstallation needs to be performed via sharepoint url as an example:


( Replace 'example' with your SharePoint domain and 'testSite' with the connected 'Classic website')
 https://example.sharepoint.com/sites/testSite/_catalogs/solutions/Forms/AllItems.aspx.

 There is a shortcut on Provisioning that opens a link to your Solutions Gallery. Just click on the info icon on its row.
 
 Every table title provides a quick link to see if the desired changes were performed correctly.

![analyzer_installed.png](../image/analyzer_installed.png)