This section is available by clicking the Analyzer menu item followed by selecting the Permissions option.

### **Permissions**


Information related directly with the set of actions that you can perform using Provisioning or in manual processes on SharePoint.

There are seven permissions being shown at Provisioning:

- Tenant App Catalog
	- Checks if the App Catalog exists in the tenant SharePoint.	

- Tenant Admin
	- Checks if the user has admin permissions in the tenant SharePoint.

- Site App Catalog
	- Checks if the Site Catalog exists in the **root** of the connected SharePoint url.

- Site Admin
	- Checks if the user has admin permissions in the **root** of the connected SharePoint url.

- Custom Scripts
	- Checks if the custom scripts are enabled of the connected site ( Mainly used for the classic sites ).

- Installing on Tenant
	- Checks if it is possible to install a product in the Tenant App Catalog

- Installing on Site Collection
	- Checks if it is possible to install a product in the connected Site Collection App Catalog.

### Actions

Actions are also available on this tab. They represent what you can execute based on permissions that you have.  
The following table represents all the current available actions:

| Action | Tenant App Catalog | Tenant Admin | Site App Catalog  |  Site Admin   | Custom Scripts |
|-----| :---: | :---: | :---: | :---: |  :---:  |
| Modern Theme Tenant | X |  |  |  |  |
| Modern Theme Tenant Full Color Experience | X | X |  |  |  |
| Modern Theme Site App Catalog |  |  | X | X |  |
| Modern Theme No Site App Catalog | X | X | X | X |  |
| Modern Theme Site Full Color Experience && No Site App Catalog | X | X | X | X |  |
| Modern Theme Site No Full Color Experience && No Site App Catalog | X | X | X | X |  |
| Modern Theme Site - No Color && Site App Catalog |  |  | X | X |  |
| Modern Web Tenant | X |  |  |  |  |
| Modern Web Part Site |  |  | X | X |  |
| Modern Web Part Site - No Site App Catalog | X | X | X | X |  |
| Classic - Theme Site |  |  |  | X | X |
| Classic - Web Part Site |  |  |  | X | X |

---

What exactly means each action of the table above?

- Modern Theme Tenant
	- Deploys a Modern Theme in the tenant App Catalog.

- Modern Theme Tenant Full Color Experience
	- Deploys a Modern Theme in the tenant App Catalog that contains full color experience 

- Modern Site App Catalog
	- Deploys a Modern Theme into the Site App Catalog of the connected SharePoint Site

- Modern Theme No Site App Catalog
	- Deploys a Modern Theme into the Site App Catalog after creating the Site App Catalog,

- Modern Theme Site Full Color Experience && No Site App Catalog
	- Deploys a Modern Theme with full color experience into the Site App Catalog after creating the Site App Catalog.

- Modern Theme Site No Full Color Experience && No Site App Catalog
	- Deploys a Modern Theme Site with no full color experience and after creating the Site App Catalog.

- Modern Theme Site - No Color && Site App Catalog
	- Deploys a Theme Site without full color experience when the Site App Catalog is already created.

- Modern Web Tenant
	- Deploy a BindTuning Solution into the Tenant App Catalog.

- Modern Web Part Site
	- Deploys a BindTuning Web Part in the Site App Catalog.

- Modern Web Part Site - No Site App Catalog
	- Deploys a BindTuning Web Part in the Site App Catalog after creating the Site App catalog.

- Classic - Theme Site
	- Deploys a Theme Site into a classic SharePoint Site in its solutions gallery.

- Classic - Web Part Site
	- Deploys a Web Part into a classic SharePoint Site in its solutions gallery.


![permissions.png](../image/analyzer_permissions.png)


