This section is available by clicking the Analyzer menu item followed by selecting the Templates History option.

The Templates History provides information about all the templates that were installed into the SharePoint URL and its subsites on a timeline table.

<p class="alert alert-info">This feature is not available on the Tenant Admin Site.</p>

The information shown for each template is:

-	Name ( Name of the Project )
-	Date ( Date of the installation )
-   Status ( If the instalation was succeed or not ) 
-	Site ( Which site the template was installed ) 
-   Version ( The version of the template )
-   Report ( Additional information about the installation such as errors )

![templatesHistory.png](../image/analyzer_templatesHistory.png)