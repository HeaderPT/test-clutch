### Operative System

BindTuning Provisioning work for:

- Windows 7 Service Pack 1 or Above
 
<p class="alert alert-sucess">Windows Management Framework v4.0 needs to be installed before using BindTuning Provisioning. This can be found in the following link: <a href="http://www.microsoft.com/en-us/download/details.aspx?id=40855" target="_blank">Windows Management Framework 4.0</a></p>

___
### Platforms supported

- SharePoint Online (Office 365)
- SharePoint 2016
- SharePoint 2013

<p class="alert alert-warning">To use Provisioning with SharePoint On-Prem, the SharePoint Server Client Components SDK must be installed.</p>

<p>This can be found in the following links:</p>
<ul>
    <li><a href="https://www.microsoft.com/en-us/download/details.aspx?id=51679" target="_blank">SharePoint Server 2016 Client Components SDK</a></li>
    <li><a href="https://www.microsoft.com/en-us/download/details.aspx?id=35585" target="_blank">SharePoint Server 2013 Client Components SDK</a></li>
</ul>

___
### Enabling PowerShell scripts

If you have issues passing the login step, the system might not be able to run PowerShell scripts from unknown sources, so you must authorize it.

1.	Search for **Windows PowerShell** command-line, right click and **Run as Administrator**;
2.	[**Optional for step 5**] Run **Get-ExecutionPolicy** to find the current policy
	- Save the **Policy_Name**
3.	Run **Set-ExecutionPolicy Unrestricted** to enable the system to run scripts;
4.	Type **A** to select All and then press **Enter**
	- Your system now meets the requirements to correctly run the **PowerShell scripts**
5.	Optionally run **Set-ExecutionPolicy** [**Policy_Name**]. On the step 2 was saved the Policy_Name in order to set the default configurations now. This should be done when you don't need to use the Provisioning.

More information: <a href="https://technet.microsoft.com/en-us/library/ee176961.aspx" target="_blank">Windows PowerShell Script Execution Policy</a>