**Enable Sandbox solutions** <small>For Installing BindTuning Products</small>

If you are trying to activate a sandbox solution on SharePoint 2016 and your activation button is disabled follow the steps bellow.

<p class="alert alert-warning">Note: that in order to activate a Sandbox solution you need to be a Site Collection admin and the Sandbox execution service needs to be running in your server.</p>

1. Open the Central Administration;
2. Go to **Application Management** -> **Manage Services on Server**;
3. Make sure that the **Microsoft SharePoint Foundation Sandbox Code Service** is running;
4. If the server is not running, you will notice that is not possible to activate it directly from the services page. To start the service, go to **System Settings** -> **Manage Services in this Farm**;
5. Locate **Microsoft SharePoint Foundation Sandboxed Code Service** and **Enable Auto Provision**;
6. If the provision fails open the console SharePoint Management Shell and execute this command:

```powershell
Get-SPServiceInstance -server $env:COMPUTERNAME | where-object {$_.TypeName -eq "Microsoft SharePoint Foundation Sandboxed Code Service"} | Start-SPServiceInstance -confirm:$false > $null
```
7. In your server open the services window and check if the **SharePoint User Code Host** is running.


If the service is started and the Activate button is not yet active you will need to elevate the privileges In the WebApplication UserPolicy, add the Service Account used for Microsoft SharePoint Foundation Sandboxed Code Service with Full Control rights.

1. Open the Central Administration
2. Go to **Security** -> **Configure Service Accounts**
3. Select the **Windows Service – Microsoft SharePoint Foundation SandBox Code** Service
4. Select an account with Full Control rights


**Deactivate Solutions** <small>For Installing BindTuning Products</small>

Provisioning can't deativate and uninstall manually activated solutions. We recommend deactivating solutions that you will install in the future with the Provisioning.

1. Open the settings menu and click on **Site settings**;
2. Under **Web Designer Galleries**, click on **Solutions**
3. Select an the uploaded solution and click on **Deactivate**;
4. Execute this steps for each solution that you'll install with the Provisioning
