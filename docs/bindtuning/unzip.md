You will download a file where the Provisioning is stored. The name of the file is **ProvisioningEngineInstaller.zip**.

1. Extract the .zip content;
2. Go to folder where you have extracted the BindTuning Provisioning;
3. Execute the **exe** file; 
4. The Provisioning will automatically install and will be ready to be used. An icon will be created on your desktop and also on the Start menu.

![clutchzip.png](../image/clutchzip.png)