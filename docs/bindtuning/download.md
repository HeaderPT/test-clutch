First step is to download the BindTuning Provisioning.

1. Inside your BindTuning account, go to **My Downloads** and switch to the **Provisioning** tab;
2. Mouse hover the **Provisioning** and click on **View Provisioning**;
3. On the BindTuning Provisioning page details click on **Download**.

![account.png](../image/account.png)